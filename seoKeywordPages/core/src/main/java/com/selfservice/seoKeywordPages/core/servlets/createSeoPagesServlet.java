package com.selfservice.seoKeywordPages.core.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.WCMException;

/**
 * @author Sapient
 */
@Component(service = Servlet.class, immediate = true, property = {
		Constants.SERVICE_DESCRIPTION + "=Create SEO Keyword Pages Servlet",
		"sling.servlet.methods=" + HttpConstants.METHOD_POST,
		"sling.servlet.paths=" + "/bin/seokeywordpages/createLandingPage" })
public class createSeoPagesServlet extends SlingAllMethodsServlet {
	private static final long serialVersionUID = 1668099305241096740L;

	private static final Logger log = LoggerFactory.getLogger(createSeoPagesServlet.class);

	static final String SKELETON_PAGE_PATH = "/content/unilever/skeletonPage";
	static final String DESTINATION_PATH = "/home";
	static final String TWO_HUNDRED = "200";
	static final String FOUR_HUNDRED_FOUR = "404";
	static final String FOUR_HUNDRED_FIVE = "405";
	static final String FOUR_HUNDRED_NINE = "409";
	private static Session session;

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {

		log.debug("Inside Servlet");

		HashMap<String, String> returnMap = new HashMap<>();

		String htmlResponse = "";
		String errorResponse = "";

		String localePath = request.getParameter("locale");

		String labelHeader = request.getParameter("labelHeader");

		String headerSectionNav = request.getParameter("headerSectionNav");

		log.debug(" locale: " + localePath);
		
		Map<String, String[]> keywordList = new HashMap<>();

		for (int i = 0;; i++) {

			String keyword = request.getParameter("keywordsList/item" + i + "/keyword");
			String[] tags = request.getParameterValues("keywordsList/item" + i + "/tags");
			if (keyword != null && tags != null) {
				keywordList.put(keyword, tags);
			} else {
				break;
			}
		}

		String pagePath = localePath.concat(DESTINATION_PATH);
		log.debug("pagePath: " + pagePath);

		ResourceResolver resolver;
		resolver = request.getResourceResolver();

		try {
			for (Map.Entry<String, String[]> entry : keywordList.entrySet()) {
				String keyword = entry.getKey();
				String[] tags = entry.getValue();
				createPages(resolver, pagePath, labelHeader, headerSectionNav, keyword, tags, returnMap);

				if(returnMap.containsValue(FOUR_HUNDRED_FOUR) && returnMap.containsValue(FOUR_HUNDRED_FIVE))
				{ break ;}
			}

			if (returnMap.containsValue(TWO_HUNDRED) && !returnMap.containsValue(FOUR_HUNDRED_FOUR)
					&& !returnMap.containsValue(FOUR_HUNDRED_FIVE) && !returnMap.containsValue(FOUR_HUNDRED_NINE)) {

				htmlResponse = "<html>\r\n" + "<head>\r\n" + "    <title>OK</title>\r\n" + "</head>\r\n"
						+ "    <body>\r\n" + "    <h1>OK</h1>\r\n" + "    <table>\r\n" + "        <tbody>\r\n"
						+ "            <tr>\r\n" + "                <td>Status</td>\r\n"
						+ "                <td><div id=\"Status\">200</div></td>\r\n" + "            </tr>\r\n"
						+ "            <tr>\r\n" + "                <td>Message</td>\r\n"
						+ "                <td><div id=\"Message\">Page created</div></td>\r\n"
						+ "            </tr>\r\n" + "            <tr>\r\n" + "                <td>Location</td>\r\n"
						+ "                <td><a href=\"" + pagePath + "\" id=\"Location\">" + pagePath
						+ "</a></td>\r\n" + "            </tr>\r\n" + "            <tr>\r\n"
						+ "                <td>Parent Location</td>\r\n" + "                <td><a href=\"" + pagePath
						+ "\" id=\"ParentLocation\">" + pagePath + "</a></td>\r\n" + "            </tr>\r\n"
						+ "            <tr>\r\n" + "                <td>Path</td>\r\n"
						+ "                <td><div id=\"Path\">" + pagePath + "</div></td>\r\n"
						+ "            </tr>\r\n" + "            <tr>\r\n" + "                <td>Referer</td>\r\n"
						+ "                <td><a href=\"\" id=\"Referer\"></a></td>\r\n" + "            </tr>\r\n"
						+ "            <tr>\r\n" + "                <td>ChangeLog</td>\r\n"
						+ "                <td><div id=\"ChangeLog\">&lt;pre&gt;&lt;/pre&gt;</div></td>\r\n"
						+ "            </tr>\r\n" + "        </tbody>\r\n" + "    </table>\r\n"
						+ "    <p><a href=\"\">Go Back</a></p>\r\n" + "    <p><a href=\"" + pagePath
						+ "\">Modified Resource</a></p>\r\n" + "    <p><a href=\"" + pagePath
						+ "\">Parent of Modified Resource</a></p>\r\n" + "    </body>\r\n" + "</html>\r\n" + "";

				response.setStatus(HttpServletResponse.SC_OK, "Pages Created");
				response.getWriter().write(htmlResponse);
			}
			else if(returnMap.containsValue(FOUR_HUNDRED_FIVE)){
				response.setStatus(HttpServletResponse.SC_NOT_FOUND, "<p><b>Skeleton Page Not Found</b></p>");
			}
			else if(returnMap.containsValue(FOUR_HUNDRED_FOUR)){
				response.setStatus(HttpServletResponse.SC_NOT_FOUND, "<p><b>Destination Path Not Found</b></p>");
			}
			else {
				
				
				for (Map.Entry<String, String> entry : returnMap.entrySet()) {
					
					
					 switch(entry.getValue()) {
				      case "200":
				    	  errorResponse = errorResponse.concat("<p>").concat(entry.getKey()).concat(": <b>Page Created</b></p>");
				        break;
				    
				      case "409":
				    	  errorResponse = errorResponse.concat("<p>").concat(entry.getKey()).concat(": <b>Page Already Exist</b></p>");
					    break;
				    }
					
				}
			
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST, errorResponse);
			}

			session.save();

			response.setCharacterEncoding("UTF-8");
			response.setContentType("text/html");
			response.getWriter().flush();
		}
		catch (NullPointerException e) {

			log.error("null pointer error: {}", e);

		} catch (Exception e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST, e.getLocalizedMessage());
			log.debug("error: {} ", e);

		}

	}

	protected void createPages(ResourceResolver resolver, String pagePath, String labelHeader, String headerSectionNav,
			String seoKeyword, String[] keywordTags, HashMap<String, String> returnMap) {

		String user = "";

		String name = seoKeyword.replaceAll("\\s", "").toLowerCase();
		log.debug("name: " + name);
		String newPagePath = pagePath.concat("/").concat(name);

		Page newPage;
		PageManager pageManager;

		try {

			session = resolver.adaptTo(Session.class);
			user = resolver.getUserID();
			log.debug("user:  " + user);

			// create a page manager instance
			pageManager = resolver.adaptTo(PageManager.class);

			Page skeletonPage = pageManager.getContainingPage(resolver.getResource(SKELETON_PAGE_PATH));

			if (skeletonPage != null) {

				if (pageManager.getContainingPage(resolver.getResource(pagePath)) != null) {

					newPage = pageManager.getContainingPage(resolver.getResource(newPagePath));
					if (newPage == null) {
						// create a new page
						newPage = pageManager.copy(skeletonPage, newPagePath, name, false, false, true);

						if (newPage != null) {

							Node newNode = newPage.adaptTo(Node.class);
							Node jcrContentNode = newNode.getNode("jcr:content");
							if (jcrContentNode != null) {

								jcrContentNode.setProperty("jcr:title", seoKeyword);
								jcrContentNode.setProperty("cq:tags", keywordTags);
								log.debug("jcrContentNode path: " + jcrContentNode.getPath());

								Node richtextv2 = jcrContentNode.getNode("flexi_hero_par/richtextv2");
								if (richtextv2 != null) {

									richtextv2.setProperty("headingText","<h1>"+labelHeader+"</h1>");
									

								}
								Node productListingV2 = jcrContentNode.getNode("flexi_hero_par/productlistingv2");
								if (productListingV2 != null) {

									productListingV2.setProperty("matchingTags", keywordTags);

								}
								log.debug("productNode: " + productListingV2.getPath());
								Node relatedarticles = jcrContentNode.getNode("flexi_hero_par/relatedarticles");
								if (relatedarticles != null) {
									relatedarticles.setProperty("matchingTags", keywordTags);

								}
								Node sectionnavigationv2 = jcrContentNode.getNode("flexi_hero_par/sectionnavigationv2");
								if (sectionnavigationv2 != null) {

									if (!headerSectionNav.equals("")) {
										sectionnavigationv2.setProperty("heading", headerSectionNav);
									}
								}

								session.save();
								returnMap.put(newPagePath, TWO_HUNDRED);
							}
						}

					} else {
						// page already exist
						returnMap.put(newPagePath, FOUR_HUNDRED_NINE);
					}
				} else {
					// destination path not found
					returnMap.put(newPagePath, FOUR_HUNDRED_FOUR);
				}
			} else {
				// skeleton page not found
				returnMap.put(newPagePath, FOUR_HUNDRED_FIVE);
			}
		} catch (WCMException e) {

			log.error("error: {}", e);

		} catch (PathNotFoundException e) {

			log.error("error: {}", e);

		} catch (RepositoryException e) {

			log.error("error: {}", e);

		}

	}
}