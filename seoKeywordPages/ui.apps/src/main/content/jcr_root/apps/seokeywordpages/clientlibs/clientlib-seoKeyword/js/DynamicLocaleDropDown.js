$(document).ready(function() {


    var locale = $('.localeSelect select')

    //disable tag field

    $('#tagId').prop('disabled', true);

    $('#shell-propertiespage-saveactivator coral-button-label').text("Generate Landing Pages")


    $('body').on('change', '.brandSelect', function() {
        if ($("body").attr("is-opened-once") &&
            $("body").attr("is-opened-once").length > 0) {
            $.ajax({
                type: "GET",
                url: '/etc/acmselfservice/seoPageCreation.html',
                async: false,
                success: function(response) {
                    var responseHtml = $(response);
                    var existingValue = $(".brandSelect").val();
                    $("div.foundation-layout-panel").replaceWith($(responseHtml.find("div")[0]).parent());
                    $(".brandSelect").val(existingValue);
                    $('#shell-propertiespage-saveactivator coral-button-label').text("Generate Landing Pages")
                }
            });

        }
        var brandText = $('option:selected', this).text();
        var brandValue = $('option:selected', this).val();


        getLocale(brandValue);

        setTagRootPath(brandText.toLowerCase());
        //setTagDialog();
    });
    $(".coral-Form-fieldwrapper #tagId button.coral-Button").on('click', function() {
        var width = 0;
        var id = setInterval(frame, 10);

        function frame() {
            if (width == 100) {
                clearInterval(id);
            } else {
                if ($("coral-dialog.granite-pickerdialog.coral-Dialog.is-open").height() &&
                    $("coral-dialog.granite-pickerdialog.coral-Dialog.is-open").height() > 0) {
                    //$(".coral-Form-fieldwrapper #tagId").attr("is-opened-once","true");
                    $("body").attr("is-opened-once", "true");
                    clearInterval(id);
                }
                width++;

            }
        }

    });
});
//end of document ready


function getLocale(brandPath) {

    var url = '/bin/seokeywordpages/getLocaleDropDownJson?brandPath=' + brandPath

    //console.log(url);

    $.ajax({

        'url': '/bin/seokeywordpages/getLocaleDropDownJson',
        'type': 'GET',
        'data': {
            'brandPath': brandPath
        },
        'success': function(data) {
            console.log('Data: ' + JSON.stringify(data));
            createDropdown(data);
        },
        'error': function(request, error) {
            alert("Request: " + JSON.stringify(request));
        }
    });

} //end of getLocale



function createDropdown(data) {

    //console.log("create");
    var localeSelect = $('.localeSelect select')
    var localeCoralSelect = $('.localeSelect coral-selectlist')

    localeCoralSelect.empty();

    localeSelect.append('<option selected="true" >Select Locale</option>');
    localeSelect.prop('selectedIndex', 0);

    localeCoralSelect.append('<coral-select-item selected="true" >Select Locale</coral-select-item>');
    localeCoralSelect.prop('selectedIndex', 0);

    $.each(data, function(key, entry) {

        //console.log("loop: "+entry.path + entry.name);
        localeSelect.append($('<option></option>').attr('value', entry.path).text(entry.name));
        localeCoralSelect.append($('<coral-select-item></coral-select-item>').attr('value', entry.path).text(entry.name));
    })

} //end of createDropdown


function setTagRootPath(brandName) {

    var pickersrc = $('#tagId').attr('pickersrc')
    var brandVal = $('#tagId').attr('brandVal')
    var pickerSrcSuggestion = $('#tagId coral-overlay').attr('data-foundation-picker-buttonlist-src')

    if (brandVal == null | brandVal == 'undefined') {

        $('#tagId').prop('disabled', false);

        var pickersrcArray = pickersrc.split("&selectionCount");
		 var pickersrcSuggestionArray = pickerSrcSuggestion.split("{&query}");

        pickersrcArray = pickersrcArray[0].concat("%2f").concat(brandName).concat("&selectionCount").concat(pickersrcArray[1])
		pickersrcSuggestionArray = pickersrcSuggestionArray[0].concat("%2f").concat(brandName).concat("{&query}").concat(pickersrcSuggestionArray[1])

        //console.log(pickersrcArray);
		//set new url for tag 
        var pickersrc = $('#tagId').attr('pickersrc', pickersrcArray)
        var pickerSrcSuggestion = $('#tagId coral-overlay').attr('data-foundation-picker-buttonlist-src', pickersrcSuggestionArray)
        var brandVal = $('#tagId').attr('brandVal', brandName)
    } else {


        var pickersrcReplace = pickersrc.replace(brandVal, brandName)
        var pickersrc = $('#tagId').attr('pickersrc', pickersrcReplace)

        var pickersrcSuggestionReplace = pickerSrcSuggestion.replace(brandVal, brandName)
        var pickerSrcSuggestion = $('#tagId coral-overlay').attr('data-foundation-picker-buttonlist-src', pickersrcSuggestionReplace)
        var brandVal = $('#tagId').attr('brandVal', brandName)

    }

} // end of setTagRootPath