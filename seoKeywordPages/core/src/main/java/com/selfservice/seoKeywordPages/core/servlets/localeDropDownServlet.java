package com.selfservice.seoKeywordPages.core.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.api.wrappers.ValueMapDecorator;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.ui.components.ds.DataSource;
import com.adobe.granite.ui.components.ds.EmptyDataSource;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * @author Sapient DynamicDropDown Servlet to display dropdown for Rates product
 */
@Component(service = Servlet.class, immediate = true, property = {
		Constants.SERVICE_DESCRIPTION + "=Locale Json Servlet", "sling.servlet.methods=" + HttpConstants.METHOD_GET,
		"sling.servlet.paths=" + "/bin/seokeywordpages/getLocaleDropDownJson" })
public class localeDropDownServlet extends SlingSafeMethodsServlet {
	private static final long serialVersionUID = 1668099305241096740L;

	private static final Logger log = LoggerFactory.getLogger(localeDropDownServlet.class);

	@Reference
	QueryBuilder builder;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) {
		String path = request.getParameter("brandPath");
		String resultJson;
		try {
			ResourceResolver resolver = request.getResourceResolver();
			Session session = resolver.adaptTo(Session.class);
			request.setAttribute(DataSource.class.getName(), EmptyDataSource.instance());

			// create query description as hash map 
			Map<String, String> map = new HashMap<String, String>();

			map.put("path", path);
			map.put("path.flat", "true");
			map.put("type", "cq:Page"); 
			map.put("p.hits", "selective");
			map.put("p.properties", "jcr:content/jcr:title");
			map.put("p.offset", "0"); // set offset
			map.put("p.limit", "-1"); // set limit

			Query query = builder.createQuery(PredicateGroup.create(map), session);
			query.setStart(0);

			SearchResult result = query.getResult();

			JsonArray hitsArray = new JsonArray();
			for (Hit hit : result.getHits()) {
				JsonObject hitObject = new JsonObject();

				log.debug("Hits: title {0} path {1} name {2}",hit.getTitle(),hit.getPath(),hit.getResource().getName());
				hitObject.addProperty("title", hit.getTitle());
				hitObject.addProperty("path", hit.getPath());
				hitObject.addProperty("name", hit.getResource().getName());
				hitsArray.add(hitObject);
			}
			Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
			resultJson = gson.toJson(hitsArray);

			response.setCharacterEncoding("UTF-8");
			response.setContentType("application/json");
			response.getWriter().print(resultJson.toString());
			response.getWriter().flush();
		} catch (RepositoryException | IOException e) {

			log.error("Exception occured: {} ",e);
		}
	}
}