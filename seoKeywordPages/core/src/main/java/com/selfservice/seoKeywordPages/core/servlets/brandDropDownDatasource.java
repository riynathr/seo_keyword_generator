package com.selfservice.seoKeywordPages.core.servlets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceMetadata;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.api.wrappers.ValueMapDecorator;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.ui.components.ds.DataSource;
import com.adobe.granite.ui.components.ds.EmptyDataSource;
import com.adobe.granite.ui.components.ds.SimpleDataSource;
import com.adobe.granite.ui.components.ds.ValueMapResource;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;

/**
 * @author Sapient DynamicDropDown Servlet to display dropdown for Rates product
 */
@Component(service = Servlet.class, immediate = true, property = {
		Constants.SERVICE_DESCRIPTION + "=Dynamic DropDown Servlet",
		"sling.servlet.methods=" + HttpConstants.METHOD_GET,
		"sling.servlet.paths=" + "/bin/seokeywordpages/getBrandsDropDown" })

public class brandDropDownDatasource extends SlingSafeMethodsServlet {
	private static final long serialVersionUID = 1668099305241096740L;

	private static final Logger log = LoggerFactory.getLogger(brandDropDownDatasource.class);
	
	@Reference
	QueryBuilder builder;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) {
		List<Resource> dropDownList = new ArrayList<>();
		ResourceResolver resolver = request.getResourceResolver();
		Session session = resolver.adaptTo(Session.class);
		request.setAttribute(DataSource.class.getName(), EmptyDataSource.instance());
		ValueMap dropDownMap = null;
		
		// create query as hash map for fetching brand
		Map<String, String> map = new HashMap<String, String>();

		
		map.put("path", "/content");
		map.put("path.flat", "true");
		map.put("type", "cq:Page");
		map.put("1_property", "jcr:content/sling:resourceType");
		map.put("1_property.value", "iea/pagecomponents/flexibleLayoutBasePage");
		map.put("1_property.operation", "like");
		map.put("p.hits", "selective");
		map.put("p.properties", "jcr:content/jcr:title");
		map.put("p.offset", "0"); // set offset
		map.put("p.limit", "-1"); //set limit

		Query query = builder.createQuery(PredicateGroup.create(map), session);
		

		SearchResult result = query.getResult();

		
		try {
			dropDownMap = new ValueMapDecorator(new HashMap<String, Object>());
			dropDownMap.put("text","Select Brand");//set static value
			dropDownMap.put("value","");
			dropDownList.add(new ValueMapResource(request.getResourceResolver(), new ResourceMetadata(), "nt:unstructured",
					dropDownMap));
			log.debug("hits: "+result.getHits().toString());
			// iterating over the results
			for (Hit hit : result.getHits()) {
				
				dropDownMap = new ValueMapDecorator(new HashMap<String, Object>());
				String path = hit.getPath();
				dropDownMap.put("text", hit.getProperties().get("jcr:title"));

				dropDownMap.put("value", hit.getPath());
				log.debug("hit" + hit.toString() + hit.getProperties().get("jcr:title") + path);
				dropDownList.add(new ValueMapResource(request.getResourceResolver(), new ResourceMetadata(), "nt:unstructured",
						dropDownMap));
			}

		} catch (RepositoryException e) {
			log.error("Exception occured: {} ",e);
		}

		

		DataSource dataSource = new SimpleDataSource(dropDownList.iterator());
		request.setAttribute(DataSource.class.getName(), dataSource);
	}
}